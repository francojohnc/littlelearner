package com.example.rammersd.littlelearner.science;


import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.Constant;
import com.example.rammersd.littlelearner.R;

public class SciencePartsOfTheBodyClick extends BaseFragment {
    private TextView txtTitle;
    private TextView txtDesciption;
    private ImageView image;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_science_clicked, container, false);
        cast(v);
        setValues();
        return v;
    }

    private void setValues() {
        String extraArray[]= getArguments().getStringArray(Constant.SciencekeyTitle);
        txtTitle.setText(extraArray[0]);
        txtDesciption.setText(extraArray[1]);
        image.setImageResource(getArguments().getInt(Constant.SciencekeyImage));
    }

    private void cast(View v) {
        txtTitle=(TextView)v.findViewById(R.id.txtTitle);
        txtDesciption=(TextView)v.findViewById(R.id.txtDesciption);
        image=(ImageView) v.findViewById(R.id.image);
    }
    @Override
    public int bgMusic() {
        return R.raw.sound_lesson;
    }
}
