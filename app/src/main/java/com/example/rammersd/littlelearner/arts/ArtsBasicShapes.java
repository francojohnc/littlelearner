package com.example.rammersd.littlelearner.arts;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.Constant;
import com.example.rammersd.littlelearner.R;
import com.example.rammersd.littlelearner.classes.UtilFragment;


public class ArtsBasicShapes extends BaseFragment {
    private Button btnRectangle;
    private Button btnOval;
    private Button btnDiamond;
    private Button btnHeart;
    private Button btnStar;
    private Button btnTriangle;
    private Button btnSquare;
    private Button btnCircle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.arts_basic_shapes, container, false);
        cast(v);
        buttonClick();
        return v;
    }

    private void buttonClick() {
        //Rectangle//
        btnRectangle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.rectangle);
                bundle.putInt(Constant.ArtskeyImage, Constant.rectangleImg);
                bundle.putInt(Constant.keyIsSound, Constant.rectangleSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Oval//
        btnOval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.oval);
                bundle.putInt(Constant.ArtskeyImage, Constant.ovalImg);
                bundle.putInt(Constant.keyIsSound, Constant.ovalSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Diamond//
        btnDiamond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.diamond);
                bundle.putInt(Constant.ArtskeyImage, Constant.diamondImg);
                bundle.putInt(Constant.keyIsSound, Constant.diamondSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Heart//
        btnHeart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.heart);
                bundle.putInt(Constant.ArtskeyImage, Constant.heartImg);
                bundle.putInt(Constant.keyIsSound, Constant.heartSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Star//
        btnStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.star);
                bundle.putInt(Constant.ArtskeyImage, Constant.starImg);
                bundle.putInt(Constant.keyIsSound, Constant.starSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Triangle//
        btnTriangle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.triangle);
                bundle.putInt(Constant.ArtskeyImage, Constant.triangleImg);
                bundle.putInt(Constant.keyIsSound, Constant.triangleSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Square//
        btnSquare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.square);
                bundle.putInt(Constant.ArtskeyImage, Constant.squareImg);
                bundle.putInt(Constant.keyIsSound, Constant.squareSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Circle//
        btnCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.circle);
                bundle.putInt(Constant.ArtskeyImage, Constant.circleImg);
                bundle.putInt(Constant.keyIsSound, Constant.circleSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
    }

    private void cast(View v) {
        btnRectangle = (Button)v.findViewById(R.id.btnRectangle);
        btnOval = (Button)v.findViewById(R.id.btnOval);
        btnDiamond = (Button)v.findViewById(R.id.btnDiamond);
        btnHeart = (Button)v.findViewById(R.id.btnHeart);
        btnStar = (Button)v.findViewById(R.id.btnStar);
        btnTriangle = (Button)v.findViewById(R.id.btnTriangle);
        btnSquare = (Button)v.findViewById(R.id.btnSquare);
        btnCircle = (Button)v.findViewById(R.id.btnCircle);
    }

    @Override
    public int bgMusic() {
        return R.raw.sound_lesson;
    }
}
