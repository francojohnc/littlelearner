package com.example.rammersd.littlelearner;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.rammersd.littlelearner.fragments.MenuFragment;

public abstract class BaseFragment extends Fragment {
    private AudioHandler audio;
    protected static final String tag = BaseFragment.class.getName();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        audio = new AudioHandler(getActivity());
        if(Constant.isSound)audio.playMusicLoop(bgMusic());
    }
    public abstract int bgMusic();
    @Override
    public void onResume() {
        super.onResume();
        audio.resume();
        Fragment currentFragment = getActivity().getFragmentManager().findFragmentById(R.id.frame_container);
        if (currentFragment instanceof MenuFragment) {
            ((MainActivity)getActivity()).hideButton();
        }else{
            ((MainActivity)getActivity()).showButton();
        }
    }
    protected void playSound(int sound){
        audio.addSound(sound, sound);
        audio.playSound(sound);
    }
    protected void stopSound(int sound){
        audio.stopSound(sound);
    }
    @Override
    public void onPause() {
        super.onPause();
        audio.pause();
    }
    public void toggleMusic(){
        audio.toggleMusic();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        audio.stopAll();
    }
    protected Bitmap takeScreenshot(Activity activity) {
        ViewGroup decor = (ViewGroup) activity.getWindow().getDecorView();
        ViewGroup decorChild = (ViewGroup) decor.getChildAt(0);
        decorChild.setDrawingCacheEnabled(true);
        decorChild.buildDrawingCache();
        Bitmap drawingCache = decorChild.getDrawingCache(true);
        Bitmap bitmap = Bitmap.createBitmap(drawingCache);
        decorChild.setDrawingCacheEnabled(false);
        return bitmap;
    }
    protected void back(){
        FragmentManager fragmentManager = getFragmentManager();
        int backStackEntryCount = fragmentManager.getBackStackEntryCount();
        if (backStackEntryCount > 1) {
            fragmentManager.popBackStack();
        }
    }
}
