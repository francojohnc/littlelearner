package com.example.rammersd.littlelearner.science;

import android.app.Fragment;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.Constant;
import com.example.rammersd.littlelearner.R;
import com.example.rammersd.littlelearner.classes.UtilFragment;


public class ScienceFiveSenses extends BaseFragment {
    private Button btnHearing;
    private Button btnTouch;
    private Button btnSight;
    private Button btnTaste;
    private Button btnSmell;
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.science_five_senses, container, false);
        cast(v);
        buttonClick();
        return v;
    }

    private void buttonClick() {
        //Hearing//
        btnHearing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.SciencekeyTitle, Constant.hearing);
                bundle.putInt(Constant.SciencekeyImage, Constant.hearingImg);
                //fragment
                UtilFragment.fragmentAddToBackStack(new SciencePartsOfTheBodyClick(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Touch//
        btnTouch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.SciencekeyTitle, Constant.touch);
                bundle.putInt(Constant.SciencekeyImage, Constant.touchImg);
                //fragment
                UtilFragment.fragmentAddToBackStack(new SciencePartsOfTheBodyClick(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Sight//
        btnSight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.SciencekeyTitle, Constant.sight);
                bundle.putInt(Constant.SciencekeyImage, Constant.sightImg);
                //fragment
                 UtilFragment.fragmentAddToBackStack(new SciencePartsOfTheBodyClick(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Taste//
        btnTaste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.SciencekeyTitle, Constant.taste);
                bundle.putInt(Constant.SciencekeyImage, Constant.tasteImg);
                //fragment
                UtilFragment.fragmentAddToBackStack(new SciencePartsOfTheBodyClick(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Smell//
        btnSmell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.SciencekeyTitle, Constant.smell);
                bundle.putInt(Constant.SciencekeyImage, Constant.smellImg);
                //fragment
                UtilFragment.fragmentAddToBackStack(new SciencePartsOfTheBodyClick(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });

    }

    private void cast(View v) {
        btnHearing = (Button)v.findViewById(R.id.btnHearing);
        btnTouch = (Button)v.findViewById(R.id.btnTouch);
        btnSight = (Button)v.findViewById(R.id.btnSight);
        btnTaste = (Button)v.findViewById(R.id.btnTaste);
        btnSmell = (Button)v.findViewById(R.id.btnSmell);

    }

    @Override
    public int bgMusic() {
        return R.raw.sound_lesson;
    }
}
