package com.example.rammersd.littlelearner;


public class Constant {
    public static final String keyImages="keyImages";
    public static final String keyTitle="keyTitle";
    public static final String keyIsSound="keyIsSound";
    public static final String keySpeak="keySpeak";
    public static final String SciencekeyTitle="SciencekeyTitle";
    public static final String SciencekeyImage="SciencekeyImage";
    public static final String ArtskeyTitle="ArtskeyTitle";
    public static final String ArtskeyImage="ArtskeyImage";
    public static boolean isSound=true;
    // English Alphabet Order //
    public static final int alphabetOrderImgs[]= {R.drawable.a,R.drawable.b,R.drawable.c,R.drawable.d,R.drawable.e,R.drawable.f,R.drawable.g,R.drawable.h,R.drawable.i,R.drawable.j, R.drawable.k,R.drawable.l,R.drawable.m,R.drawable.n,R.drawable.o,R.drawable.p,R.drawable.q,R.drawable.r,R.drawable.s,R.drawable.t,R.drawable.u,R.drawable.v,R.drawable.w,R.drawable.x,R.drawable.y,R.drawable.z};
    public static final String alphabetOrderSpeak[]={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};

    // English A,B,C Example //
    public static final int abcExampleImgs[]= {R.drawable.apple,R.drawable.ball,R.drawable.cat,R.drawable.duck,R.drawable.elepant,R.drawable.fan,R.drawable.grapes,R.drawable.hat,R.drawable.ice_cream,R.drawable.jungle, R.drawable.kite,R.drawable.lion,R.drawable.monkey,R.drawable.nest,R.drawable.orange,R.drawable.pig,R.drawable.queen,R.drawable.rainbow,R.drawable.star,R.drawable.train,R.drawable.umbrella,R.drawable.vase,R.drawable.whale,R.drawable.xylophone,R.drawable.yoyo,R.drawable.zebra};
    public static final String abcExampleSpeak[]={"Apple","Ball","Cat","Duck","Elepant","Fan","Grapes","Hat","Ice Cream","Jungle","Kite","Lion","Monkey","Nest","Orange","Pig","Queen","Rainbow","Star","Train","Umbrella","Vase","Whale","Xylophone","Yoyo","Zebra"};

    //Math Counting Numbers //
    public static final int countingNumbersImgs[]= {R.drawable.c1,R.drawable.c2,R.drawable.c3,R.drawable.c4,R.drawable.c5,R.drawable.c6,R.drawable.c7,R.drawable.c8,R.drawable.c9,R.drawable.c10};
    public static final String countingNumbersSpeak[]= {"One","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten"};

    // Math Numbers Example //
    public static final int numberExampleImgs[]= {R.drawable.one,R.drawable.two,R.drawable.three,R.drawable.four,R.drawable.five,R.drawable.six,R.drawable.seven,R.drawable.eight,R.drawable.nine,R.drawable.ten};
    public static final String[] numberExampleSpeak= {"One Bear","Two Horse","Three Bird","Four Butterfly","Five Turle","Six Snail","Seven Frog","Eight Sheep","Nine Fish","Ten Cow"};

    // Filipino Abakada //
    public static final int abakadaImg[]= {R.drawable.abakada_a,R.drawable.abakada_b,R.drawable.abakada_k,R.drawable.abakada_d,R.drawable.abakada_e,R.drawable.abakada_g,R.drawable.abakada_h,R.drawable.abakada_i,R.drawable.abakada_l,R.drawable.abakada_m,R.drawable.abakada_n,R.drawable.abakada_ng,R.drawable.abakada_o,R.drawable.abakada_p,R.drawable.abakada_r,R.drawable.abakada_s,R.drawable.abakada_t,R.drawable.abakada_u,R.drawable.abakada_w,R.drawable.abakada_y};
    public static final int abakadaSpeak[]= {R.raw.sound_aa,R.raw.sound_ba,R.raw.sound_ka,R.raw.sound_da,R.raw.sound_ee,R.raw.sound_ga,R.raw.sound_ha,R.raw.sound_ii,R.raw.sound_la,R.raw.sound_ma,R.raw.sound_na,R.raw.sound_nga,R.raw.sound_oo,R.raw.sound_pa,R.raw.sound_ra,R.raw.sound_sa,R.raw.sound_ta,R.raw.sound_uu,R.raw.sound_wa,R.raw.sound_ya};

    // Filipino A,Ba,Ka,Da Example //
    public static final int abakadaExampleImgs[]= {R.drawable.a_aso,R.drawable.b_bola,R.drawable.d_dahon,R.drawable.e_elepante,R.drawable.g_gatas,R.drawable.h_halaman,R.drawable.i_ilaw,R.drawable.k_kabayo,R.drawable.l_lapis,R.drawable.m_manika,R.drawable.ng_ngipin,R.drawable.n_nanay,R.drawable.o_orasan,R.drawable.p_payong,R.drawable.r_radyo,R.drawable.s_sapatos,R.drawable.t_tasa,R.drawable.u_unan,R.drawable.w_watawat,R.drawable.y_yoyo};
    public static final int abakadaExampleSound[]= {R.raw.sound_abakada_aso,R.raw.sound_abakada_bola,R.raw.sound_abakada_dahon,R.raw.sound_abakada_elepante,R.raw.sound_abakada_gatas,R.raw.sound_abakada_halaman,R.raw.sound_abakada_ilaw,R.raw.sound_abakada_kabayo,R.raw.sound_abakada_lapis,R.raw.sound_abakada_manika,R.raw.sound_abakada_ngipin,R.raw.sound_abakada_nanay,R.raw.sound_abakada_orasan,R.raw.sound_abakada_payong,R.raw.sound_abakada_radyo,R.raw.sound_abakada_sapatos,R.raw.sound_abakada_tasa,R.raw.sound_abakada_unan,R.raw.sound_abakada_watawat,R.raw.sound_abakada_yoyo};

    // Arts Basic Shapes //
    //Rectangle//
    public static final String rectangle[]={"RECTANGLE"};
    public static final int rectangleImg=R.drawable.shapes_rectangle;
    public static final int rectangleSound=R.raw.rectangle;
    //Oval//
    public static final String oval[]={"OVAL"};
    public static final int ovalImg=R.drawable.shapes_oval;
    public static final int ovalSound=R.raw.oval;
    //Diamond//
    public static final String diamond[]={"DIAMOND"};
    public static final int diamondImg=R.drawable.shapes_diamond;
    public static final int diamondSound=R.raw.diamond;
    //Heart//
    public static final String heart[]={"HEART"};
    public static final int heartImg=R.drawable.shapes_heart;
    public static final int heartSound=R.raw.heart;
    //Star//
    public static final String star[]={"STAR"};
    public static final int starImg=R.drawable.shapes_star;
    public static final int starSound=R.raw.star;
    //Triangle//
    public static final String triangle[]={"TRIANGLE"};
    public static final int triangleImg=R.drawable.shapes_triangle;
    public static final int triangleSound=R.raw.triangle;
    //Square//
    public static final String square[]={"SQUARE"};
    public static final int squareImg=R.drawable.shapes_square;
    public static final int squareSound=R.raw.square;
    //Circle//
    public static final String circle[]={"CIRCLE"};
    public static final int circleImg=R.drawable.shapes_circle;
    public static final int circleSound=R.raw.circle;

    // Arts Colors //
    //White//
    public static final String white[]={"WHITE"};
    public static final int whiteImg=R.drawable.color_white;
    public static final int whiteSound=R.raw.white;
    //Yellow//
    public static final String yellow[]={"YELLOW"};
    public static final int yellowImg=R.drawable.color_yellow;
    public static final int yellowSound=R.raw.yellow;
    //Purple//
    public static final String purple[]={"PURPLE"};
    public static final int purpleImg=R.drawable.color_purple;
    public static final int purpleSound=R.raw.purple;
    //Pink//
    public static final String pink[]={"PINK"};
    public static final int pinkImg=R.drawable.color_pink;
    public static final int pinkSound=R.raw.pink;
    //Red//
    public static final String red[]={"RED"};
    public static final int redImg=R.drawable.color_red;
    public static final int redSound=R.raw.red;
    //Orange//
    public static final String orange[]={"ORANGE"};
    public static final int orangeImg=R.drawable.color_orange;
    public static final int orangeSound=R.raw.orange;
    //Green//
    public static final String green[]={"GREEN"};
    public static final int greenImg=R.drawable.color_green;
    public static final int greenSound=R.raw.green;
    //Grey//
    public static final String grey[]={"GREY"};
    public static final int greyImg=R.drawable.color_grey;
    public static final int greySound=R.raw.grey;
    //Brown//
    public static final String brown[]={"BROWN"};
    public static final int brownImg=R.drawable.color_brown;
    public static final int brownSound=R.raw.brown;
    //Blue//
    public static final String blue[]={"BLUE"};
    public static final int blueImg=R.drawable.color_blue;
    public static final int blueSound=R.raw.blue;

    // Science Animals //
    public static final int animalsImgs[]= {R.drawable.animal_bird,R.drawable.animal_cat,R.drawable.animal_dog,R.drawable.animal_duck,R.drawable.animal_elephant,R.drawable.animal_horse,R.drawable.animal_lion,R.drawable.animal_pig,R.drawable.animal_rooster,R.drawable.animal_sheep};
    public static final int animalsSound[]= {R.raw.sound_bird,R.raw.sound_cat,R.raw.sound_dog,R.raw.sound_duck,R.raw.sound_elephante,R.raw.sound_horse,R.raw.sound_lion,R.raw.sound_pig,R.raw.sound_rooster,R.raw.sound_sheep};

    // Science Parts of the Body //
    //Head//
    public static final String head[]={"HEAD","The upper part of the human body and containing the brain, mouth, and sense organs."};
    public static final int headImg=R.drawable.parts_of_the_body_head;
    //Shoulder//
    public static final String shoulder[]={"SHOULDER","The upper joint of the human arm and the part of the body between this and the neck."};
    public static final int shoulderImg=R.drawable.parts_of_the_body_shoulder;
    //Leg//
    public static final String leg[]={"LEG","Each of the limbs on which a person or animal walks and stands."};
    public static final int legImg=R.drawable.parts_of_the_body_leg;
    //Toe//
    public static final String toe[]={"TOE","Any of the five digits at the end of the human foot."};
    public static final int toeImg=R.drawable.parts_of_the_body_toe;
    //Arm//
    public static final String arm[]={"ARM","Each of the two upper limbs of the human body from the shoulder to the hand."};
    public static final int armImg=R.drawable.parts_of_the_body_arm;
    //Knee//
    public static final String knee[]={"KNEE","The joint between the thigh and the lower leg in humans."};
    public static final int kneeImg=R.drawable.parts_of_the_body_knee;

    // Science Parts of the Computer //
    //Monitor
    public static final String monitor[]={"MONITOR","Electronic device that shows pictures."};
    public static final int monitorImg=R.drawable.computer_monitor;
    //System Unit//
    public static final String systemUnit[]={"SYSTEM UNIT","The enclosure for all the other main interior components of a computer."};
    public static final int systemUnitImg=R.drawable.computer_system_unit;
    //Mouse//
    public static final String mouse[]={"MOUSE","Items can be moved or selected by pressing the mouse buttons (called clicking)."};
    public static final int mouseImg=R.drawable.computer_mouse;
    //Keyboard//
    public static final String keyboard[]={"KEYBOARD","Typewriter-style device which uses an arrangement of buttons or keys to act as mechanical levers or electronic switches."};
    public static final int keyboardImg=R.drawable.computer_keyboard;

    // Five Senses //
    //Hearing//
    public static final String hearing[]={"HEARING","Our ears are used for hearing."};
    public static final int hearingImg=R.drawable.five_senses_hearing;
    //Touch//
    public static final String touch[]={"TOUCH","Our hands are used for touching."};
    public static final int touchImg=R.drawable.five_senses_touch;
    //Sight//
    public static final String sight[]={"SIGHT","Our eyes are used for seeing."};
    public static final int sightImg=R.drawable.five_senses_sight;
    //Taste//
    public static final String taste[]={"TASTE","Our tounge is used for tasting."};
    public static final int tasteImg=R.drawable.five_senses_taste;
    //Smell
    public static final String smell[]={"SMELL","Our nose is used for smelling."};
    public static final int smellImg=R.drawable.five_senses_smell;

    // Writing //
    public static final int writingImgAlphabet[]={R.drawable.writing_a,R.drawable.writing_b,R.drawable.writing_c,R.drawable.writing_d,R.drawable.writing_e,R.drawable.writing_f,R.drawable.writing_g,R.drawable.writing_h,R.drawable.writing_i,R.drawable.writing_j,R.drawable.writing_k,R.drawable.writing_l,R.drawable.writing_m,R.drawable.writing_n,R.drawable.writing_o,R.drawable.writing_p,R.drawable.writing_q,R.drawable.writing_r,R.drawable.writing_s,R.drawable.writing_t,R.drawable.writing_u,R.drawable.writing_v,R.drawable.writing_w,R.drawable.writing_x,R.drawable.writing_y,R.drawable.writing_z};
    public static final int writingImgNumberImg[]={R.drawable.writing_1,R.drawable.writing_2,R.drawable.writing_3,R.drawable.writing_4,R.drawable.writing_5,R.drawable.writing_6,R.drawable.writing_7,R.drawable.writing_8,R.drawable.writing_9,R.drawable.writing_10};

    // All Exercises //
    //Science exercises//
    public static final String scienceExercise[][]={
            //parts of the body
            {R.drawable.parts_exercise_arm+"","arm",R.drawable.parts_exercise_head+"","head",R.drawable.parts_exercise_toe+"","toe"},//exercise parts of body 1
            {R.drawable.parts_exercise_knee+"","knee",R.drawable.parts_exercise_shoulder+"","shoulder",R.drawable.parts_exercise_leg+"","leg"},//exercise parts of body 2
            //Animals
            {R.drawable.animals_exercise_dog+"","dog",R.drawable.animals_exercise_rat+"","rat",R.drawable.animals_exercise_snake+"","snake"},//exercise animals 1
            {R.drawable.animals_exercise_cow+"","cow",R.drawable.animals_exercise_rabbit+"","rabbit",R.drawable.animals_exercise_monkey+"","monkey"},//exercise animals 2
            {R.drawable.animals_exercise_tiger+"","tiger",R.drawable.animals_exercise_butterfly+"","butterfly",R.drawable.animals_exercise_lion+"","lion"},//exercise animals 3
            {R.drawable.animals_exercise_elephant+"","elephant",R.drawable.animals_exercise_zebra+"","zebra",R.drawable.animals_exercise_cat+"","cat"},//exercise animals 4
    };
    //Science Exercises Desc
    public static final String scienceExerciseDesc="Write the corresponding name of images";

    //Filipino exercises//
    public static final String filipinoExercise[][]={
            {R.drawable.filipino_exercises_dahon+"","dahon",R.drawable.filipino_exercises_ilaw+"","ilaw",R.drawable.filipino_exercises_nanay+"","nanay"}, //exercise filipino 1
            {R.drawable.filipino_exercises_aso+"","aso",R.drawable.filipino_exercises_kabayo+"","kabayo",R.drawable.filipino_exercises_orasan+"","orasan"}, //exercise filipino 2
            {R.drawable.filipino_exercises_gatas+"","gatas",R.drawable.filipino_exercises_lapis+"","lapis",R.drawable.filipino_exercises_manika+"","manika"}, //exercise filipino 3
            {R.drawable.filipino_exercises_payong+"","payong",R.drawable.filipino_exercises_radyo+"","radyo",R.drawable.filipino_exercises_sapatos+"","sapatos"}, //exercise filipino 4
    };
    //Filipino Exercises Desc
    public static final String filipinoExerciseDesc="Write the corresponding name of images";

    //English exercises//
    public static final String englishExercise[][]={
            {R.drawable.english_exercises_apple+"","apple",R.drawable.english_exercises_lion+"","lion",R.drawable.english_exercises_ball+"","ball"}, //exercise filipino 1
            {R.drawable.english_exercises_cat+"","cat",R.drawable.english_exercises_fan+"","fan",R.drawable.english_exercises_queen+"","queen"}, //exercise filipino 2
            {R.drawable.english_exercises_duck+"","duck",R.drawable.english_exercises_kite+"","kite",R.drawable.english_exercises_monkey+"","monkey"}, //exercise filipino 3
            {R.drawable.english_exercises_train+"","train",R.drawable.english_exercises_whale+"","whale",R.drawable.english_exercises_yoyo+"","yoyo"}, //exercise filipino 4
    };
    //English Exercises Desc
    public static final String englishExerciseDesc="Write the corresponding name of images";

    //Arts exercises//
    public static final String artsExercise[][]={
            //Shape
            {R.drawable.shape_exercise_circle+"","circle",R.drawable.shape_exercise_heart+"","heart",R.drawable.shape_exercise_rectangle+"","rectangle"}, //exercise arts 1
            {R.drawable.shape_exercise_diamond+"","diamond",R.drawable.shape_exercise_oval+"","oval",R.drawable.shape_exercise_square+"","square"}, //exercise arts 2
            {R.drawable.shape_exercise_triangle+"","triangle",R.drawable.shape_exercise_star+"","star",R.drawable.shape_exercise_diamond+"","diamond"}, //exercise arts 3
            //Colors
            {R.drawable.color_exercise_blue+"","blue",R.drawable.color_exercise_brown+"","brown",R.drawable.color_exercise_green+"","green"}, //exercise colors 1
            {R.drawable.color_exercise_grey+"","grey",R.drawable.color_exercise_orange+"","orange",R.drawable.color_exercise_red+"","red"}, //exercise colors 2
            {R.drawable.color_exercise_pink+"","pink",R.drawable.color_exercise_violet+"","violet",R.drawable.color_exercise_yellow+"","yellow"}, //exercise colors 3
            {R.drawable.color_exercise_white+"","white",R.drawable.color_exercise_brown+"","brown",R.drawable.color_exercise_grey+"","grey"}, //exercise colors 4

    };
    //Arts Exercises Desc
    public static final String artsExerciseDesc="Write the corresponding color and shape of images";

    //Math exercises//
    public static final String mathExercise[][]={
            {R.drawable.math_exercise_8cow+"","eight",R.drawable.math_exercise_8bird+"","eight",R.drawable.math_exercise_2snake+"","two"}, //exercise 1
            {R.drawable.math_exercise_7doll+"","seven",R.drawable.math_exercise_6jar+"","six",R.drawable.math_exercise_3dog+"","three"}, //exercise 2
            {R.drawable.math_exercise_4fish+"","four",R.drawable.math_exercise_2sheep+"","two",R.drawable.math_exercise_tiger+"","one"}, //exercise 3
    };
    //Math Exercises Desc
    public static final String mathExerciseDesc="Write the corresponding number of images";

    // Music Let's Sing //
    public static final String videos[][]={
            //English
            {"A,Ba,Ka,Da Song", R.raw.abakada+""},
            {"ABC song", R.raw.abc_song+""},
            {"Ako Ay May Lobo Song", R.raw.ako_ay_may_lobo+""},
            {"Bahay Kubo Song", R.raw.bahay_kubo_song+""},
            {"Happy Birthday Song", R.raw.happy_birthday_song+""},
            {"Leron Leron Sinta Song", R.raw.leron+""},
            {"Old Macdonald Had A Farm Song", R.raw.macdonald_farm_song+""},
            {"Pen Pen De Sarapen Song", R.raw.penpen_sarapen_song+""},
            {"Rain Rain Go Away Song", R.raw.rain_go_away_song+""},
            {"Twinkle Twinkle Little Star Song", R.raw.twinkle_little_song+""},
            //Tagalog





    };
}
