package com.example.rammersd.littlelearner;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.SoundPool;
import android.util.SparseIntArray;

public class AudioHandler {
	private SoundPool sound;
	private int soundMax;
	private float volumeLeft = 1.0f;
	private float volumeRight = 1.0f;
	private MediaPlayer music;
	private SparseIntArray audioPool;
	public AudioManager audioManager;
	private Context context;
	public AudioHandler(Context context) {
	    this.context = context;
	    soundMax = 2; 
	    audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
	    sound = new SoundPool(soundMax, AudioManager.STREAM_MUSIC, 0);
	    audioPool = new SparseIntArray();
	    music = new MediaPlayer();
	}
	public void addSound(int resID, int soundID) {   
	    int streamID = -1;
	    audioPool.put(soundID, sound.load(context, resID, 1));
	    do{streamID = sound.play(audioPool.get(soundID), 0, 0, 1, 0, 1f);}
	    while(streamID==0);
	}
	public void playSound(int soundID) {
	    sound.play((Integer)audioPool.get(soundID),volumeLeft,volumeRight, 1, 0, 1f);
	}
	public void stopSound(int soundID){
		sound.stop((Integer)audioPool.get(soundID));
		sound.autoPause();
	}
	public void playMusic(int soundID) {
		music = MediaPlayer.create(context, soundID);
		music.start();
	}
	public void playMusicLoop(int soundID) {
		music = MediaPlayer.create(context, soundID);
		music.setLooping(true);
		music.start();
	}
	public void playStream(String url) {
		try {
			music.setDataSource(url); 
			music.prepare();
		} catch (Exception e) {
			e.printStackTrace();
		}
		music.start();
	}
	public void playStreamLoop(String url) {
		try {
			music.setDataSource(url); 
			music.prepare();
		} catch (Exception e) {
			e.printStackTrace();
		}
		music.setLooping(true);
		music.start();
	}
	public void musicSeekTo(int playPosition) {
		if(music.isPlaying()){
			int playPositionInMillisecconds = (music.getDuration() / 100) * playPosition;
			music.seekTo(playPositionInMillisecconds);
		}
	}
	public int musicGetCurrentPosition(){
		return (int)(((float)music.getCurrentPosition()/music.getDuration())*100);
	}
	public boolean musicIsPlaying(){
		return music.isPlaying();
	}
	public void toggleMusic(){//pause and start 
		if (music.isPlaying())music.pause();
		else music.start();
	}
	public void setMasterVolume( float volumeLeft, float volumeRight )
	{
		this.volumeLeft = volumeLeft;
		this.volumeRight = volumeRight;
		music.setVolume( volumeLeft, volumeRight );
	}
	public void stopAll(){
		for (int i = 0; i < audioPool.size(); i++){sound.stop(i);}
		music.stop();
	}
	public void resume(){
		music.start();
	}
	public void pause(){
		music.pause();
	}
	public void getBuffer(OnBufferingUpdateListener OnBufferingUpdateListener){
		music.setOnBufferingUpdateListener(OnBufferingUpdateListener);
	}
}