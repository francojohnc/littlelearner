package com.example.rammersd.littlelearner.classes;


import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;

import java.util.HashMap;
import java.util.Map;

public class UtilFragment {
    public static void fragmentAddToBackStack(Fragment fragment,FragmentManager getSupportFragmentManager,int frame_container){
        getSupportFragmentManager.beginTransaction().replace(frame_container, fragment).addToBackStack(fragment.getClass().getName()).commit();
        //getSupportFragmentManager().popBackStack();
    }
    public static void fragmentAddToBackStack(Fragment fragment,FragmentManager getSupportFragmentManager,int frame_container,Bundle bundle){
        fragment.setArguments(bundle);
        getSupportFragmentManager.beginTransaction().replace(frame_container, fragment).addToBackStack(fragment.getClass().getName()).commit();
    }
}
