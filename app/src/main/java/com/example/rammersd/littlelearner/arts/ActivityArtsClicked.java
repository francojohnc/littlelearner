package com.example.rammersd.littlelearner.arts;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.Constant;
import com.example.rammersd.littlelearner.R;

public class ActivityArtsClicked extends BaseFragment {
    private TextView ArtstxtTitle;
    private ImageView Artsimage;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_arts_clicked, container, false);
        cast(v);
        setValues();
        imgClick();
        return v;
    }

    private void imgClick() {
        Artsimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playSound(getArguments().getInt(Constant.keyIsSound));
            }
        });
    }

    private void setValues() {
        String extraArray[]= getArguments().getStringArray(Constant.ArtskeyTitle);
        ArtstxtTitle.setText(extraArray[0]);
        Artsimage.setImageResource(getArguments().getInt(Constant.ArtskeyImage));
    }

    private void cast(View v) {
        ArtstxtTitle=(TextView)v.findViewById(R.id.artsTxtTitle);
        Artsimage=(ImageView) v.findViewById(R.id.artsImage);
    }

    @Override
    public int bgMusic() {
        return R.raw.sound_lesson;
    }
}

