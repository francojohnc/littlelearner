package com.example.rammersd.littlelearner;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class InstructionFragment extends BaseFragment {
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_instruction, container, false);
        return v;
    }

    @Override
    public int bgMusic() {
        return R.raw.sound_menu;
    }
}