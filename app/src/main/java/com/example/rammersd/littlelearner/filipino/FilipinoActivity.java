package com.example.rammersd.littlelearner.filipino;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rammersd.littlelearner.AudioHandler;
import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.Constant;
import com.example.rammersd.littlelearner.Exercises;
import com.example.rammersd.littlelearner.OrderActivity;
import com.example.rammersd.littlelearner.R;
import com.example.rammersd.littlelearner.classes.UtilFragment;
import com.example.rammersd.littlelearner.science.SciencePartsOfTheBody;
import com.example.rammersd.littlelearner.science.SciencePartsOfTheBodyClick;


public class FilipinoActivity extends BaseFragment {
    private Button btnAbakada;
    private Button btnAbakadaExample;
    private Button btnExercises;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.filipino_activity, container, false);
        cast(v);
        buttonClick();
        return v;
    }

    private void cast(View v) {
        btnAbakada = (Button) v.findViewById(R.id.AbakadaFilipino);
        btnAbakadaExample = (Button) v.findViewById(R.id.AbakadaExample);
        btnExercises = (Button) v.findViewById(R.id.exercisesFilipino);
    }

    private void buttonClick() {
        //Abakada//
        btnAbakada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putIntArray(Constant.keyImages, Constant.abakadaImg);
                bundle.putIntArray(Constant.keySpeak, Constant.abakadaSpeak);
                bundle.putBoolean(Constant.keyIsSound, true);
                //fragment
                UtilFragment.fragmentAddToBackStack(new OrderActivity(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Abakada Example
        btnAbakadaExample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putIntArray(Constant.keyImages, Constant.abakadaExampleImgs);
                bundle.putIntArray(Constant.keySpeak, Constant.abakadaExampleSound);
                bundle.putBoolean(Constant.keyIsSound, true);
                //fragment
                UtilFragment.fragmentAddToBackStack(new OrderActivity(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Exercises//
        btnExercises.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //fragment
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constant.keyImages, Constant.filipinoExercise);
                bundle.putString(Constant.keyTitle, Constant.filipinoExerciseDesc);
                UtilFragment.fragmentAddToBackStack(new Exercises(), getFragmentManager(), R.id.frame_container,bundle);
            }
        });

    }
    public int bgMusic() {
        return R.raw.sound_menu;
    }
}
