package com.example.rammersd.littlelearner.activity;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;


public class VideoPlayerActivity extends Activity{
	private MediaController mediaControls;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setupSreen();
		setContentView(getLayout());
	}
	private RelativeLayout getLayout(){
		RelativeLayout layout = new RelativeLayout(this);
		VideoView videoView = new VideoView(this);
		//add param
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
		layout.addView(videoView,params);
		initVideo(videoView);
		return layout;
	}
	private void setupSreen() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	}
	private void initVideo(VideoView videoView) {
		mediaControls = new MediaController(VideoPlayerActivity.this);
		try {
			videoView.setMediaController(mediaControls);
			videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + getIntent().getIntExtra("video", 0)));
			videoView.start();
		} catch (Exception e) {
		}
	}
}
