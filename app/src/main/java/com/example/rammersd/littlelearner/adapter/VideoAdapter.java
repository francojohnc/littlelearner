package com.example.rammersd.littlelearner.adapter;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.rammersd.littlelearner.Constant;
import com.example.rammersd.littlelearner.R;
import com.example.rammersd.littlelearner.activity.VideoPlayerActivity;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder> {
    private Context context;

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        context = viewGroup.getContext();
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final ViewHolder v = new ViewHolder(inflater.inflate(R.layout.item_video, viewGroup, false));
        v.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(v.itemView.getContext(), VideoPlayerActivity.class);
                i.putExtra("video", Integer.parseInt(Constant.videos[v.getAdapterPosition()][1]));
                v.itemView.getContext().startActivity(i);
            }
        });
        return v;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.recipe.setText(Constant.videos[position][0]);
    }

    @Override
    public int getItemCount() {
        return Constant.videos.length;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView recipe;
        public ViewHolder(View v) {
            super(v);
            recipe = (TextView) v.findViewById(R.id.recipe);
        }
    }
}
