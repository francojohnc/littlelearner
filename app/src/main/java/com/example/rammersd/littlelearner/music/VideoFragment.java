package com.example.rammersd.littlelearner.music;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.R;
import com.example.rammersd.littlelearner.adapter.VideoAdapter;



public class VideoFragment extends BaseFragment {
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_video, container, false);
        recyClerView(v);
        setAdapter();
        return v;
    }
    private void recyClerView(View v) {
        recyclerView = (RecyclerView) v.findViewById(R.id.recyler);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
    }
    private void setAdapter() {
        VideoAdapter adapter = new VideoAdapter();
        recyclerView.setAdapter(adapter);
    }

    @Override
    public int bgMusic() {
        return R.raw.sound_lesson;
    }
}
