package com.example.rammersd.littlelearner;

import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;


public class Exercises extends BaseFragment {
    private Button btnShare;
    private Button btnBack;
    private ImageView img[] = new ImageView[3];
    private TextView txt[] = new TextView[3];
    private TextView txtScore;
    private TextView txtTitle;
    private int randomNum = 0;
    private int score = 0;
    private String Exercise[][];
    private String title="";
    private int arrayCount[]={1,3,5};
    private ArrayList<Integer>shuffle=new ArrayList<>(Arrays.asList(1,3,5));
    private String scoreDesc[]={"Need improvement","Good","Very good","Excellent"};
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_exercises, container, false);
        shuffleArray();
        getExtra();
        cast(v);
        setValue();
        buttonClick();
        setOnTouchListener();
        setOnDragListener();
        return v;
    }

    private void setOnDragListener() {
        for (int i = 0; i < txt.length; i++) {
            final int count = i;
            txt[count].setOnDragListener(new View.OnDragListener() {
                @Override
                public boolean onDrag(View v, DragEvent event) {
                    switch (event.getAction()) {
                        case DragEvent.ACTION_DRAG_STARTED:
                            Log.d(tag,"ACTION_DRAG_STARTED");
                            break;
                        case DragEvent.ACTION_DRAG_ENTERED:
                            Log.d(tag,"ACTION_DRAG_ENTERED");
                            break;
                        case DragEvent.ACTION_DRAG_EXITED:
                            Log.d(tag,"ACTION_DRAG_EXITED");
                            break;
                        case DragEvent.ACTION_DRAG_LOCATION:
                            Log.d(tag,"ACTION_DRAG_LOCATION");
                            break;
                        case DragEvent.ACTION_DRAG_ENDED:
                            Log.d(tag, "ACTION_DRAG_ENDED");
                            getScore();
//                            imgVisible();
                            break;
                        case DragEvent.ACTION_DROP:
                            // get text value
                            TextView txt = (TextView) v;
                            Log.d(tag, txt.getText().toString());
                            //get clip label
                            String clipLabel = event.getClipDescription().getLabel().toString();
                            Log.d(tag,"clipLabel:"+clipLabel);
                            if (txt.getText().toString().equalsIgnoreCase(clipLabel))score++;
                            break;
                        default:
                            break;
                    }
                    return true;
                }
            });
        }

    }

    private void getScore() {
        txtScore.setText(scoreDesc[score]);
    }

    private void imgVisible() {
        for (int i = 0; i < img.length; i++) {
            img[i].setVisibility(View.VISIBLE);
        }
    }


    private void shuffleArray() {
        Collections.shuffle(shuffle);
    }

    private void getExtra() {
        Exercise = (String[][]) getArguments().getSerializable(Constant.keyImages);
        title =getArguments().getString(Constant.keyTitle);
    }

    private void setOnTouchListener() {
        for (int i = 0; i < img.length; i++) {
            final int count = i;
            img[count].setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    int action = event.getAction() & MotionEvent.ACTION_MASK;
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                        case MotionEvent.ACTION_POINTER_DOWN:
                            String passExercise=Exercise[randomNum][arrayCount[count]];
                            Log.d(tag,"Exercise:"+passExercise);
                            ClipData data = ClipData.newPlainText(passExercise,count+"");
                            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
                            v.startDrag(data, shadowBuilder, v, 0);
                            v.setVisibility(View.INVISIBLE);
                            break;
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_POINTER_UP:
                        case MotionEvent.ACTION_CANCEL:
                            break;
                        case MotionEvent.ACTION_MOVE:
                            break;
                    }
                    return false;
                }
            });
        }
    }

    private void buttonClick() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back();
            }
        });
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), takeScreenshot(getActivity()), "title", null);
                Uri screenshotUri = Uri.parse(path);
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                sharingIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
                sharingIntent.setType("image/png");

                startActivity(Intent.createChooser(sharingIntent, "Share Score"));
            }
        });
    }

    private void setValue() {
        //genirate random number
        randomNum = new Random().nextInt(Exercise.length);
        Log.d(tag,"randomNum:"+randomNum);
        //set title
        txtTitle.setText(title);
        //set image
        img[0].setImageResource(Integer.parseInt(Exercise[randomNum][0]));
        img[1].setImageResource(Integer.parseInt(Exercise[randomNum][2]));
        img[2].setImageResource(Integer.parseInt(Exercise[randomNum][4]));
        //set text value;
        txt[0].setText(Exercise[randomNum][shuffle.get(0)]);
        txt[1].setText(Exercise[randomNum][shuffle.get(1)]);
        txt[2].setText(Exercise[randomNum][shuffle.get(2)]);

    }

    private void cast(View v) {
        btnBack = (Button) v.findViewById(R.id.btnBack);
        btnShare = (Button) v.findViewById(R.id.btnShare);
        img[0] = (ImageView) v.findViewById(R.id.img1);
        img[1] = (ImageView) v.findViewById(R.id.img2);
        img[2] = (ImageView) v.findViewById(R.id.img3);
        //
        txt[0] = (TextView) v.findViewById(R.id.txt1);
        txt[1] = (TextView) v.findViewById(R.id.txt2);
        txt[2] = (TextView) v.findViewById(R.id.txt3);
        //
        txtScore = (TextView) v.findViewById(R.id.txtScore);
        txtTitle = (TextView) v.findViewById(R.id.txtTitle);
    }
    @Override
    public int bgMusic() {
        return R.raw.sound_exercise;
    }

}
