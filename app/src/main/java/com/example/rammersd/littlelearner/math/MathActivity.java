package com.example.rammersd.littlelearner.math;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.Constant;
import com.example.rammersd.littlelearner.Exercises;
import com.example.rammersd.littlelearner.OrderActivity;
import com.example.rammersd.littlelearner.R;
import com.example.rammersd.littlelearner.classes.UtilFragment;

public class MathActivity extends BaseFragment {
    private Button btnCountingNumbers;
    private Button btnNumberExample;
    private Button btnExercises;
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.math_activity, container, false);
        cast(v);
        buttonClicl();
        return v;

//        // Math Counting Numbers //
//        Button myCountingNumbers = (Button) findViewById(R.id.countingNumbersMath);
//        myCountingNumbers.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(getApplicationContext(), OrderActivity.class);
//                i.putExtra(Constant.keyImages,Constant.countingNumbersImgs);
//                i.putExtra(Constant.keySpeak,Constant.countingNumbersSpeak);
//                startActivity(i);
//            }
//        });
//
//        // Math Number Examples //
//        Button myNumberExamples = (Button) findViewById(R.id.numberExampleMath);
//        myNumberExamples.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(getApplicationContext(), OrderActivity.class);
//                i.putExtra(Constant.keyImages,Constant.numberExampleImgs);
//                i.putExtra(Constant.keySpeak,Constant.numberExampleSpeak);
//                startActivity(i);
//            }
//        });
//
//        // Math Exercises //
//        Button myExercises = (Button) findViewById(R.id.exercisesMath);
//        myExercises.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), MathExercises.class);
//                startActivity(intent);
//            }
//        });
    }

    private void buttonClicl() {
        //Counting Numbers//
        btnCountingNumbers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putIntArray(Constant.keyImages, Constant.countingNumbersImgs);
                bundle.putStringArray(Constant.keySpeak, Constant.countingNumbersSpeak);
                //fragment
                UtilFragment.fragmentAddToBackStack(new OrderActivity(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Numbers Example//
        btnNumberExample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putIntArray(Constant.keyImages, Constant.numberExampleImgs);
                bundle.putStringArray(Constant.keySpeak, Constant.numberExampleSpeak);
                //fragment
                UtilFragment.fragmentAddToBackStack(new OrderActivity(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Exercises//
        btnExercises.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //fragment
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constant.keyImages,Constant.mathExercise);
                bundle.putString(Constant.keyTitle, Constant.mathExerciseDesc);
                UtilFragment.fragmentAddToBackStack(new Exercises(), getFragmentManager(), R.id.frame_container,bundle);
            }
        });

    }

    private void cast(View v) {
        btnCountingNumbers = (Button)v.findViewById(R.id.countingNumbersMath);
        btnNumberExample = (Button)v.findViewById(R.id.numberExampleMath);
        btnExercises = (Button)v.findViewById(R.id.exercisesMath);
    }

    @Override
    public int bgMusic() {
        return R.raw.sound_menu;
    }
}
