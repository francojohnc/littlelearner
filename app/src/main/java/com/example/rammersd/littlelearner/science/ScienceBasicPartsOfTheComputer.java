package com.example.rammersd.littlelearner.science;

import android.app.Fragment;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.Constant;
import com.example.rammersd.littlelearner.R;
import com.example.rammersd.littlelearner.classes.UtilFragment;


public class ScienceBasicPartsOfTheComputer extends BaseFragment {
    private Button btnMonitor;
    private Button btnSystemUnit;
    private Button btnMouse;
    private Button btnKeyboard;

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.science_parts_computer, container, false);
        cast(v);
        buttonClick();
        return v;
    }

    private void buttonClick() {
        //Monitor//
        btnMonitor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.SciencekeyTitle, Constant.monitor);
                bundle.putInt(Constant.SciencekeyImage, Constant.monitorImg);
                //fragment
                UtilFragment.fragmentAddToBackStack(new SciencePartsOfTheBodyClick(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //System Unit//
        btnSystemUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.SciencekeyTitle, Constant.systemUnit);
                bundle.putInt(Constant.SciencekeyImage, Constant.systemUnitImg);
                //fragment
                UtilFragment.fragmentAddToBackStack(new SciencePartsOfTheBodyClick(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Mouse//
        btnMouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.SciencekeyTitle, Constant.mouse);
                bundle.putInt(Constant.SciencekeyImage, Constant.mouseImg);
                //fragment
                UtilFragment.fragmentAddToBackStack(new SciencePartsOfTheBodyClick(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Keyboard//
        btnKeyboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.SciencekeyTitle, Constant.keyboard);
                bundle.putInt(Constant.SciencekeyImage, Constant.keyboardImg);
                //fragment
                UtilFragment.fragmentAddToBackStack(new SciencePartsOfTheBodyClick(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });

    }

    private void cast(View v) {
        btnMonitor = (Button)v.findViewById(R.id.btnMonitor);
        btnSystemUnit = (Button)v.findViewById(R.id.btnSystemUnit);
        btnMouse = (Button)v.findViewById(R.id.btnMouse);
        btnKeyboard = (Button)v.findViewById(R.id.btnKeyboard);
    }

    @Override
    public int bgMusic() {
        return R.raw.sound_lesson;
    }
}
