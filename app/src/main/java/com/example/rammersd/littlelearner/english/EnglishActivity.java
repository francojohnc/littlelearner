package com.example.rammersd.littlelearner.english;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rammersd.littlelearner.AudioHandler;
import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.Constant;
import com.example.rammersd.littlelearner.Exercises;
import com.example.rammersd.littlelearner.OrderActivity;
import com.example.rammersd.littlelearner.R;
import com.example.rammersd.littlelearner.classes.UtilFragment;
import com.example.rammersd.littlelearner.science.SciencePartsOfTheBody;

public class EnglishActivity extends BaseFragment {
    private Button btnAlphabetOrder;
    private Button btnAbcExample;
    private Button btnExercises;
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.english_activity, container, false);
        cast(v);
        buttonClick();
        return v;
    }

    private void cast(View v) {
        btnAlphabetOrder = (Button)v.findViewById(R.id.alphabetOrderEnglish);
        btnAbcExample = (Button)v.findViewById(R.id.abcExampleEnglish);
        btnExercises = (Button)v.findViewById(R.id.exercisesEnglish);
    }

    private void buttonClick() {
        //Alphabet Order//
        btnAlphabetOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putIntArray(Constant.keyImages, Constant.alphabetOrderImgs);
                bundle.putStringArray(Constant.keySpeak, Constant.alphabetOrderSpeak);
                //fragment
                UtilFragment.fragmentAddToBackStack(new OrderActivity(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //ABC Example//
        btnAbcExample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        //put extra
                        Bundle bundle = new Bundle();
                        bundle.putIntArray(Constant.keyImages, Constant.abcExampleImgs);
                        bundle.putStringArray(Constant.keySpeak, Constant.abcExampleSpeak);
                        //fragment
                        UtilFragment.fragmentAddToBackStack(new OrderActivity(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Exercises//
        btnExercises.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //fragment
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constant.keyImages,Constant.englishExercise);
                bundle.putString(Constant.keyTitle, Constant.englishExerciseDesc);
                UtilFragment.fragmentAddToBackStack(new Exercises(), getFragmentManager(), R.id.frame_container,bundle);
            }
        });
    }

    public int bgMusic() {
        return R.raw.sound_menu;
    }
}
