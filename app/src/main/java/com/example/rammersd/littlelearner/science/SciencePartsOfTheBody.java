package com.example.rammersd.littlelearner.science;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.Constant;
import com.example.rammersd.littlelearner.R;
import com.example.rammersd.littlelearner.classes.UtilFragment;

import java.util.zip.Inflater;


public class SciencePartsOfTheBody extends BaseFragment {
    private Button btnHead;
    private Button btnShoulder;
    private Button btnLeg;
    private Button btnToe;
    private Button btnArm;
    private Button btnKnee;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.science_parts_body, container, false);
        cast(v);
        buttonClick();
        return v;
    }

    private void cast(View v) {
        btnHead = (Button)v.findViewById(R.id.btnHead);
        btnShoulder = (Button)v.findViewById(R.id.btnShoulder);
        btnLeg = (Button)v.findViewById(R.id.btnLeg);
        btnToe = (Button)v.findViewById(R.id.btnToe);
        btnArm = (Button)v.findViewById(R.id.btnArm);
        btnKnee = (Button)v.findViewById(R.id.btnKnee);
    }

    private void buttonClick() {
        //Head//
        btnHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.SciencekeyTitle, Constant.head);
                bundle.putInt(Constant.SciencekeyImage, Constant.headImg);
                //fragment
                UtilFragment.fragmentAddToBackStack(new SciencePartsOfTheBodyClick(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });

        //Shoulder//
        btnShoulder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra//
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.SciencekeyTitle, Constant.shoulder);
                bundle.putInt(Constant.SciencekeyImage, Constant.shoulderImg);
                //fragment//
                UtilFragment.fragmentAddToBackStack(new SciencePartsOfTheBodyClick(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });

        //Leg//
        btnLeg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra//
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.SciencekeyTitle, Constant.leg);
                bundle.putInt(Constant.SciencekeyImage, Constant.legImg);
                //fragment//
                UtilFragment.fragmentAddToBackStack(new SciencePartsOfTheBodyClick(), getFragmentManager(), R.id.frame_container, bundle);

            }
        });
        //Toe//
        btnToe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra//
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.SciencekeyTitle, Constant.toe);
                bundle.putInt(Constant.SciencekeyImage, Constant.toeImg);
                //fragment//
                UtilFragment.fragmentAddToBackStack(new SciencePartsOfTheBodyClick(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Arm//
        btnArm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra//
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.SciencekeyTitle, Constant.arm);
                bundle.putInt(Constant.SciencekeyImage, Constant.armImg);
                //fragment//
                UtilFragment.fragmentAddToBackStack(new SciencePartsOfTheBodyClick(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Knee//
        btnKnee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra//
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.SciencekeyTitle, Constant.knee);
                bundle.putInt(Constant.SciencekeyImage, Constant.kneeImg);
                //fragment//
                UtilFragment.fragmentAddToBackStack(new SciencePartsOfTheBodyClick(), getFragmentManager(), R.id.frame_container, bundle);

            }
        });
    }
    @Override
    public int bgMusic() {
        return R.raw.sound_lesson;
    }
}
