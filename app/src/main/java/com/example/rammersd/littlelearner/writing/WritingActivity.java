package com.example.rammersd.littlelearner.writing;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.Constant;
import com.example.rammersd.littlelearner.R;
import com.example.rammersd.littlelearner.classes.UtilFragment;

public class WritingActivity extends BaseFragment {
    private Button btnAlphabetWriting;
    private Button btnNumbersWriting;
    private Button btnExercises;

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.writing_activity, container, false);
        cast(v);
        buttonClick();
        return v;
    }

    private void cast(View v) {
        btnAlphabetWriting = (Button)v.findViewById(R.id.alphabetWriting);
        btnNumbersWriting = (Button)v.findViewById(R.id.numbersWriting);
    }

    private void buttonClick() {
        //Alphabet Writing//
        btnAlphabetWriting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putIntArray(Constant.keyImages, Constant.writingImgAlphabet);
                UtilFragment.fragmentAddToBackStack(new WritingDraw(), getFragmentManager(), R.id.frame_container,bundle);
            }
        });
        //NumbersWriting//
        btnNumbersWriting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putIntArray(Constant.keyImages, Constant.writingImgNumberImg);
                UtilFragment.fragmentAddToBackStack(new WritingDraw(), getFragmentManager(), R.id.frame_container,bundle);
            }
        });
    }

    @Override
    public int bgMusic() {
        return R.raw.sound_menu;
    }
}
