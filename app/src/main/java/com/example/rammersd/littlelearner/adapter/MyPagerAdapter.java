package com.example.rammersd.littlelearner.adapter;


import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.example.rammersd.littlelearner.MainActivity;
import com.example.rammersd.littlelearner.R;

public class MyPagerAdapter extends PagerAdapter {
	private int images[];
	public MyPagerAdapter(int images[]) {
		this.images = images;
	}
	@Override
	public int getCount() {
		return images.length;
	}

	@Override
	public Object instantiateItem(final View container, int position) {
		LayoutInflater inflater = (LayoutInflater)container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.item_pager, null);
		ImageView img = (ImageView)view.findViewById(R.id.itemImage);
		img.setImageResource(images[position]);
		((ViewPager) container).addView(view, 0);
		view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((MainActivity)container.getContext()).pagerClick();
			}
		});
		return view;
	}
	@Override
	public void destroyItem(View arg0, int arg1, Object arg2) {
		((ViewPager) arg0).removeView((View) arg2);
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == ((View) arg1);
	}
	@Override
	public Parcelable saveState() {
		return null;
	}
}
