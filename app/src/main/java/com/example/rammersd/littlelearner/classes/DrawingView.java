package com.example.rammersd.littlelearner.classes;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;


public class DrawingView extends View {


    Context context;
    //the path that is drawn by user
    private Path drawPath;
    //drawing paint and canvas paint
    private Paint drawPaint, canvasPaint;

    public void setPaintColour(int paintColour) {
        this.paintColour = paintColour;
    }

    //intial colour
    private int paintColour = 0xffffffff;
    boolean hasDrawing;
    //0xff0d0000 black
    //canvas
    private Canvas drawCanvas;
    //canvas bitmap
    private Bitmap canvasBitmap;
    //store the current brush size and last brush size before eraser is used
    private float brushSize, eraserBrush;
    //erase checker
    private boolean erase = false;

    //constructor for DrawingView class
    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        hasDrawing = false;
        setupDrawing();
    }

    //helper method for setting up the drawing space
    private void setupDrawing() {
        float pixelAmount = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, getResources().getDisplayMetrics());
        float pixelAmountEraser = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, getResources().getDisplayMetrics());
        brushSize = pixelAmount;
        eraserBrush = pixelAmountEraser;
//		brushSize = 3;	
//		lastBrushSize = brushSize;
        drawPath = new Path();
        drawPaint = new Paint();
        drawPaint.setColor(paintColour);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(brushSize);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
        canvasPaint = new Paint(Paint.DITHER_FLAG);
        canvasPaint.setColor(0xffffffff);

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        //view given size
        super.onSizeChanged(w, h, oldw, oldh);
        canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        drawCanvas = new Canvas(canvasBitmap);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
        canvas.drawPath(drawPath, drawPaint);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //detect user touch
        float touchX = event.getX();
        float touchY = event.getY();
//		S_Nirva_Finish.saveBTN.setEnabled(true);
        hasDrawing = true;
        switch (event.getAction()) {
            //When user initally touches screen, start at that point
            case MotionEvent.ACTION_DOWN:
                drawPath.moveTo(touchX, touchY);
                break;
            //When user moves finger, draw that path
            case MotionEvent.ACTION_MOVE:
                drawPath.lineTo(touchX, touchY);
                break;
            //When user lifts finger, reset the path
            case MotionEvent.ACTION_UP:
                drawCanvas.drawPath(drawPath, drawPaint);
                drawPath.reset();
                break;
            default:
                return false;
        }
        invalidate();
        return true;
    }

    //Change the brush size
    public void setBrushSize(float newSize) {
        float pixelAmount = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, newSize, getResources().getDisplayMetrics());
        brushSize = pixelAmount;
        drawPaint.setStrokeWidth(brushSize);
    }

    //set last brush size
    public void setLastBrushSize(float lastSize) {
        brushSize = lastSize;
    }

    //toggle the erase function
    public void setErase(boolean isErase) {
        erase = isErase;
        if (erase) {
            drawPaint.setColor(Color.parseColor("#ffffff"));
            drawPaint.setStrokeWidth(eraserBrush);
        } else {
            drawPaint.setColor(paintColour);
            drawPaint.setStrokeWidth(brushSize);
        }
    }

    //clear the current canvas
    public void clear() {
        drawCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        invalidate();
        hasDrawing = false;
    }

    public boolean isHaveDrawing() {
        return hasDrawing;
    }
}
