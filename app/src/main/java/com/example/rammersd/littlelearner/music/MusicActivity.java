package com.example.rammersd.littlelearner.music;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.R;
import com.example.rammersd.littlelearner.classes.UtilFragment;

public class MusicActivity extends BaseFragment {
    private Button btnLetsSing;
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.music_activity, container, false);
        cast(v);
        buttonClick();
        return v;
    }

    private void cast(View v) {
        btnLetsSing = (Button)v.findViewById(R.id.letsSingMusic);
    }

    private void buttonClick() {
        //Lets Sing//
        btnLetsSing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilFragment.fragmentAddToBackStack(new VideoFragment(), getFragmentManager(), R.id.frame_container);
            }
        });
    }

    @Override
    public int bgMusic() {
        return R.raw.sound_menu;
    }
}
