package com.example.rammersd.littlelearner.science;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.Constant;
import com.example.rammersd.littlelearner.Exercises;
import com.example.rammersd.littlelearner.OrderActivity;
import com.example.rammersd.littlelearner.R;
import com.example.rammersd.littlelearner.classes.UtilFragment;

public class ScienceActivity extends BaseFragment{
    private Button btnPartsOfTheBody;
    private Button btnAnimals;
    private Button btnFiveSenses;
    private Button btnPartsComputer;
    private Button btnExercises;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.science_activity, container, false);
        cast(v);
        buttonClick();
        return v;
    }

    private void cast(View v) {
        btnPartsComputer = (Button) v.findViewById(R.id.partsComputerScience);
        btnPartsOfTheBody = (Button) v.findViewById(R.id.btnPartsOfTheBody);
        btnAnimals = (Button) v.findViewById(R.id.animalsScience);
        btnExercises = (Button) v.findViewById(R.id.exercisesScience);
        btnFiveSenses = (Button) v.findViewById(R.id.fiveSensesScience);
    }

    private void buttonClick() {
        //Parts of the Body//
        btnPartsOfTheBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilFragment.fragmentAddToBackStack(new SciencePartsOfTheBody(), getFragmentManager(), R.id.frame_container);
            }
        });
        //Five Senses//
        btnFiveSenses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilFragment.fragmentAddToBackStack(new ScienceFiveSenses(), getFragmentManager(), R.id.frame_container);
            }
        });
       //Animals//
        btnAnimals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putIntArray(Constant.keyImages, Constant.animalsImgs);
                bundle.putIntArray(Constant.keySpeak, Constant.animalsSound);
                bundle.putBoolean(Constant.keyIsSound,true);
                //fragment
                UtilFragment.fragmentAddToBackStack(new OrderActivity(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Parts Computer//
        btnPartsComputer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilFragment.fragmentAddToBackStack(new ScienceBasicPartsOfTheComputer(), getFragmentManager(), R.id.frame_container);
            }
        });
        //Exercises//
        btnExercises.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //fragment
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constant.keyImages,Constant.scienceExercise);
                bundle.putString(Constant.keyTitle, Constant.scienceExerciseDesc);
                UtilFragment.fragmentAddToBackStack(new Exercises(), getFragmentManager(), R.id.frame_container,bundle);
            }
        });
    }

    @Override
    public int bgMusic() {
        return R.raw.sound_menu;
    }
}
