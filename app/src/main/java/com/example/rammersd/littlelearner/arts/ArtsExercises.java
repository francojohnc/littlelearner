package com.example.rammersd.littlelearner.arts;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.R;


public class ArtsExercises extends BaseFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.arts_exercises, container, false);
        return v;
    }

    @Override
    public int bgMusic() {
        return R.raw.sound_exercise;
    }
}
