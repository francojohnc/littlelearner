package com.example.rammersd.littlelearner.science;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.R;


public class ScienceExercises extends BaseFragment {
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.science_exercises, container, false);
        return v;
    }

    @Override
    public int bgMusic() {
        return R.raw.sound_exercise;
    }
}
