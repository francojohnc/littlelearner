package com.example.rammersd.littlelearner.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.rammersd.littlelearner.AudioHandler;
import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.R;
import com.example.rammersd.littlelearner.arts.ArtsActivity;
import com.example.rammersd.littlelearner.classes.UtilFragment;
import com.example.rammersd.littlelearner.english.EnglishActivity;
import com.example.rammersd.littlelearner.filipino.FilipinoActivity;
import com.example.rammersd.littlelearner.math.MathActivity;
import com.example.rammersd.littlelearner.music.MusicActivity;
import com.example.rammersd.littlelearner.science.ScienceActivity;
import com.example.rammersd.littlelearner.writing.WritingActivity;

public class MenuFragment extends BaseFragment {
    private Button mybtnScience;
    private Button mybtnFilipino;
    private Button mybtnWriting;
    private Button mybtnMusic;
    private Button mybtnArts;
    private Button mybtnEnglish;
    private Button mybtnMathematics;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_menu, container, false);
        cast(v);
        buttonClick();
        return v;
    }

    private void cast(View v) {
        mybtnScience = (Button) v.findViewById(R.id.btnScience);
        mybtnFilipino = (Button) v.findViewById(R.id.btnFilipino);
        mybtnWriting = (Button) v.findViewById(R.id.btnWriting);
        mybtnMusic = (Button) v.findViewById(R.id.btnMusic);
        mybtnArts = (Button) v.findViewById(R.id.btnArts);
        mybtnEnglish = (Button) v.findViewById(R.id.btnEnglish);
        mybtnMathematics = (Button) v.findViewById(R.id.btnMathematics);


    }

    private void buttonClick() {
        //Science Menu//
        mybtnScience.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilFragment.fragmentAddToBackStack(new ScienceActivity(), getFragmentManager(), R.id.frame_container);
            }
        });

        //Filipino Menu//
        mybtnFilipino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilFragment.fragmentAddToBackStack(new FilipinoActivity(), getFragmentManager(), R.id.frame_container);
            }
        });

        //Writing Menu//
        mybtnWriting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilFragment.fragmentAddToBackStack(new WritingActivity(), getFragmentManager(), R.id.frame_container);
            }
        });

        //Music Menu//
        mybtnMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilFragment.fragmentAddToBackStack(new MusicActivity(), getFragmentManager(), R.id.frame_container);
            }
        });

        //Arts Menu//
        mybtnArts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilFragment.fragmentAddToBackStack(new ArtsActivity(), getFragmentManager(), R.id.frame_container);
            }
        });

        //English Menu//
        mybtnEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilFragment.fragmentAddToBackStack(new EnglishActivity(), getFragmentManager(), R.id.frame_container);
            }
        });

        //Mathematics Menu
        mybtnMathematics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilFragment.fragmentAddToBackStack(new MathActivity(), getFragmentManager(), R.id.frame_container);
            }
        });

    }

    @Override
    public int bgMusic() {
        return R.raw.sound_menu;
    }
}
