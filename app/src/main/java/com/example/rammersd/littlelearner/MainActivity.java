package com.example.rammersd.littlelearner;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.rammersd.littlelearner.arts.ArtsActivity;
import com.example.rammersd.littlelearner.classes.UtilFragment;
import com.example.rammersd.littlelearner.english.EnglishActivity;
import com.example.rammersd.littlelearner.filipino.FilipinoActivity;
import com.example.rammersd.littlelearner.fragments.MenuFragment;
import com.example.rammersd.littlelearner.math.MathActivity;
import com.example.rammersd.littlelearner.music.MusicActivity;
import com.example.rammersd.littlelearner.science.ScienceActivity;
import com.example.rammersd.littlelearner.writing.WritingActivity;

public class MainActivity extends AppCompatActivity {
    private Button btnHome;
    private ToggleButton btnSound;
    private Button btnInstruction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cast();
        buttonClick();

        Fragment fragment = new MenuFragment();
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();

    }

    private void buttonClick() {
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getFragmentManager().beginTransaction().replace(R.id.frame_container, new MenuFragment()).addToBackStack(null).commit();
            }
        });
        btnSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
                BaseFragment fragment = (BaseFragment) getFragmentManager().findFragmentById(R.id.frame_container);
                if (isChecked) {
                    fragment.onPause();
                    Constant.isSound = false;
                } else {
                    fragment.onResume();
                    Constant.isSound = true;
                }
            }
        });
        btnInstruction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilFragment.fragmentAddToBackStack(new InstructionFragment(), getFragmentManager(), R.id.frame_container);
            }
        });
    }

    private void cast() {
        btnHome = (Button) findViewById(R.id.btnHome);
        btnSound = (ToggleButton) findViewById(R.id.btnSound);
        btnInstruction = (Button)findViewById(R.id.btnInstruction);
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = MainActivity.this.getFragmentManager().findFragmentById(R.id.frame_container);
        if (currentFragment instanceof MenuFragment) {
            finish();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    public void pagerClick() {
        OrderActivity fragment = (OrderActivity) getFragmentManager().findFragmentById(R.id.frame_container);
        fragment.speak();
    }

    public void hideButton() {
        btnHome.setVisibility(View.GONE);
        btnSound.setVisibility(View.VISIBLE);
        btnInstruction.setVisibility(View.VISIBLE);
    }

    public void showButton() {
        btnHome.setVisibility(View.VISIBLE);
        btnSound.setVisibility(View.GONE);
        btnInstruction.setVisibility(View.GONE);

    }

}
