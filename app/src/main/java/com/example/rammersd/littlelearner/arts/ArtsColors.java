package com.example.rammersd.littlelearner.arts;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.Constant;
import com.example.rammersd.littlelearner.R;
import com.example.rammersd.littlelearner.classes.UtilFragment;


public class ArtsColors extends BaseFragment {
    private Button btnWhite;
    private Button btnYellow;
    private Button btnPurple;
    private Button btnPink;
    private Button btnRed;
    private Button btnOrange;
    private Button btnGreen;
    private Button btnGrey;
    private Button btnBrown;
    private Button btnBlue;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.arts_colors, container, false);
        cast(v);
        buttonClick();
        return v;
    }

    private void buttonClick() {
        //White//
        btnWhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.white);
                bundle.putInt(Constant.ArtskeyImage, Constant.whiteImg);
                bundle.putInt(Constant.keyIsSound, Constant.whiteSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Yellow//
        btnYellow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.yellow);
                bundle.putInt(Constant.ArtskeyImage, Constant.yellowImg);
                bundle.putInt(Constant.keyIsSound, Constant.yellowSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Purple//
        btnPurple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.purple);
                bundle.putInt(Constant.ArtskeyImage, Constant.purpleImg);
                bundle.putInt(Constant.keyIsSound, Constant.purpleSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Pink//
        btnPink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.pink);
                bundle.putInt(Constant.ArtskeyImage, Constant.pinkImg);
                bundle.putInt(Constant.keyIsSound, Constant.pinkSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Red//
        btnRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.red);
                bundle.putInt(Constant.ArtskeyImage, Constant.redImg);
                bundle.putInt(Constant.keyIsSound, Constant.redSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Orange//
        btnOrange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.orange);
                bundle.putInt(Constant.ArtskeyImage, Constant.orangeImg);
                bundle.putInt(Constant.keyIsSound, Constant.orangeSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Green//
        btnGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.green);
                bundle.putInt(Constant.ArtskeyImage, Constant.greenImg);
                bundle.putInt(Constant.keyIsSound, Constant.greenSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Grey//
        btnGrey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.grey);
                bundle.putInt(Constant.ArtskeyImage, Constant.greyImg);
                bundle.putInt(Constant.keyIsSound, Constant.greenSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Brown//
        btnBrown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.brown);
                bundle.putInt(Constant.ArtskeyImage, Constant.brownImg);
                bundle.putInt(Constant.keyIsSound, Constant.brownSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
        //Blue//
        btnBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //put extra
                Bundle bundle = new Bundle();
                bundle.putStringArray(Constant.ArtskeyTitle, Constant.blue);
                bundle.putInt(Constant.ArtskeyImage, Constant.blueImg);
                bundle.putInt(Constant.keyIsSound, Constant.blueSound);
                //fragment
                UtilFragment.fragmentAddToBackStack(new ActivityArtsClicked(), getFragmentManager(), R.id.frame_container, bundle);
            }
        });
    }

    private void cast(View v) {
        btnWhite = (Button)v.findViewById(R.id.btnWhite);
        btnYellow = (Button)v.findViewById(R.id.btnYellow);
        btnPurple = (Button)v.findViewById(R.id.btnPurple);
        btnPink = (Button)v.findViewById(R.id.btnPink);
        btnRed = (Button)v.findViewById(R.id.btnRed);
        btnOrange = (Button)v.findViewById(R.id.btnOrange);
        btnGreen = (Button)v.findViewById(R.id.btnGreen);
        btnGrey = (Button)v.findViewById(R.id.btnGrey);
        btnBrown = (Button)v.findViewById(R.id.btnBrown);
        btnBlue = (Button)v.findViewById(R.id.btnBlue);
    }

    @Override
    public int bgMusic() {
        return R.raw.sound_lesson;
    }
}
