package com.example.rammersd.littlelearner.writing;

import android.app.Fragment;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.Constant;
import com.example.rammersd.littlelearner.R;
import com.example.rammersd.littlelearner.classes.DrawingView;

public class WritingDraw extends BaseFragment {
    private LinearLayout linearLayout;
    private DrawingView drawingView;
    private Button btnLeft;
    private Button btnRight;
    private Button btnClear;
    private int images[];
    private ImageView img;
    private int setCount=0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_writing_draw, container, false);
        getExtra();
        cast(v);
        setupWriting();
        buttonClick();
        setImage();
        return v;
    }

    private void getExtra() {
        images= getArguments().getIntArray(Constant.keyImages);
    }

    private void buttonClick() {
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(setCount+1<images.length){
                    setCount++;
                    setImage();
                    drawingView.clear();
                }

            }
        });

        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(setCount>0){
                    setCount--;
                    setImage();
                    drawingView.clear();
                }

            }
        });
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawingView.clear();
            }
        });
    }

    private void cast(View v) {
        linearLayout = (LinearLayout)v.findViewById(R.id.linearLayout);
        drawingView = new DrawingView(getActivity(), null);
        btnRight = (Button)v.findViewById(R.id.btnRight);
        btnLeft = (Button)v.findViewById(R.id.btnLeft);
        btnClear = (Button)v.findViewById(R.id.btnClear);
        img =(ImageView)v.findViewById(R.id.img);
    }

    private void setupWriting() {

        linearLayout.addView(drawingView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }
    private void setImage() {
        img.setImageResource(images[setCount]);
    }

    @Override
    public int bgMusic() {
        return R.raw.sound_lesson;
    }
}
