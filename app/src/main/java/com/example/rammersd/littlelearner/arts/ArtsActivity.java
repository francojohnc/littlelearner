package com.example.rammersd.littlelearner.arts;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rammersd.littlelearner.AudioHandler;
import com.example.rammersd.littlelearner.BaseFragment;
import com.example.rammersd.littlelearner.Constant;
import com.example.rammersd.littlelearner.Exercises;
import com.example.rammersd.littlelearner.OrderActivity;
import com.example.rammersd.littlelearner.R;
import com.example.rammersd.littlelearner.classes.UtilFragment;
import com.example.rammersd.littlelearner.science.SciencePartsOfTheBody;

public class ArtsActivity extends BaseFragment {
    private Button btnBasicShapes;
    private Button btnColors;
    private Button btnExercises;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.arts_activity, container, false);
        cast(v);
        buttonClick();
        return v;
    }

    private void cast(View v) {
        btnBasicShapes = (Button) v.findViewById(R.id.basicShapesArts);
        btnColors = (Button) v.findViewById(R.id.colorsArts);
        btnExercises = (Button) v.findViewById(R.id.exercisesArts);
    }

    private void buttonClick() {
        //Basic Shapes//
        btnBasicShapes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilFragment.fragmentAddToBackStack(new ArtsBasicShapes(), getFragmentManager(), R.id.frame_container);
            }
        });
        //Colors//
        btnColors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UtilFragment.fragmentAddToBackStack(new ArtsColors(), getFragmentManager(), R.id.frame_container);
            }
        });
        //Exercises//
        btnExercises.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //fragment
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constant.keyImages,Constant.artsExercise);
                bundle.putString(Constant.keyTitle, Constant.artsExerciseDesc);
                UtilFragment.fragmentAddToBackStack(new Exercises(), getFragmentManager(), R.id.frame_container,bundle);
            }
        });
    }

    @Override
    public int bgMusic() {
        return R.raw.sound_menu;
    }
}
