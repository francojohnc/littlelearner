package com.example.rammersd.littlelearner;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.rammersd.littlelearner.adapter.MyPagerAdapter;
import com.example.rammersd.littlelearner.classes.Text2Speech;


public class OrderActivity extends BaseFragment {
    private ViewPager viewPager;
    private int images[];
    private int sounds[];
    private String speak[];
    private Text2Speech text2Speech;
    private boolean isSOund = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_order, container, false);
        getExtra();
        cast(v);
        setAdapter();
        pagerMove();
        return v;
    }

    private void pagerMove() {
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
                if (isSOund) {
                    stopSound(sounds[viewPager.getCurrentItem()]);
                }
            }
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            public void onPageSelected(int position) {
            }
        });
    }

    private void getExtra() {
        if (isSOund = getArguments().getBoolean(Constant.keyIsSound)) {
            sounds = getArguments().getIntArray(Constant.keySpeak);
        } else {
            speak = getArguments().getStringArray(Constant.keySpeak);
        }
        images = getArguments().getIntArray(Constant.keyImages);
    }
    private void setAdapter() {
        MyPagerAdapter adapter = new MyPagerAdapter(images);
        viewPager.setAdapter(adapter);
    }

    private void cast(View v) {
        viewPager = (ViewPager)v.findViewById(R.id.viewpager);
        text2Speech = new Text2Speech(getActivity());
    }

    @Override
    public int bgMusic() {
        return R.raw.sound_lesson;
    }
    public void speak() {
        if (isSOund) {
            playSound(sounds[viewPager.getCurrentItem()]);
        } else {
            if (text2Speech.isDoneSpeaking()) text2Speech.speech(speak[viewPager.getCurrentItem()]);
        }
    }
}

