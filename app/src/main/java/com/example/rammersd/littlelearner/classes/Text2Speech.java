package com.example.rammersd.littlelearner.classes;

import android.content.Context;
import android.media.AudioManager;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;

import java.util.HashMap;

@SuppressWarnings("deprecation")
public class Text2Speech implements TextToSpeech.OnInitListener, OnUtteranceCompletedListener{
	 private TextToSpeech tts;
	public Text2Speech(Context context) {
		 tts = new TextToSpeech(context, this);
	}

	public void speech(String text) {
		 HashMap<String, String> myHashAlarm = new HashMap<String, String>();
	        myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_ALARM));
	        myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "FINISHED PLAYING");
	        tts.speak(text, TextToSpeech.QUEUE_FLUSH, myHashAlarm);   
	}
	@Override
	public void onInit(int status) {
		if (status != TextToSpeech.ERROR) {
			tts.setOnUtteranceCompletedListener(this);
		}
	}
	@Override
	public void onUtteranceCompleted(String utteranceId) {
		if(utteranceId.equals("FINISHED PLAYING"))
        {
			
        }   
	}
	public boolean isDoneSpeaking() {
		if (!tts.isSpeaking()) {
			return true;
		}else{return false;}
	}
}
