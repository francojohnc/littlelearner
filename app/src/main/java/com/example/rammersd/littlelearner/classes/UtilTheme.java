package com.example.rammersd.littlelearner.classes;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.util.TypedValue;

import com.example.rammersd.littlelearner.R;


public class UtilTheme {
    // android:textColor="?colorPrimary"
    public static int getColor(Context context,int attColor){
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(attColor, typedValue, true);
        return typedValue.data;
    }
    public static Drawable getSelector(Context context){
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[] {android.R.attr.state_pressed},new ColorDrawable(getColor(context, R.attr.colorControlHighlight)));
        states.addState(new int[] {android.R.attr.state_activated},new ColorDrawable(getColor(context,R.attr.colorControlActivated)));
        states.addState(new int[] {android.R.attr.state_checked},new ColorDrawable(getColor(context,R.attr.colorControlActivated)));
        states.addState(new int[]{}, new ColorDrawable(getColor(context, R.attr.colorControlNormal)));
        return states;
    }
    public static Drawable getSelector(Context context,int statePressed,int stateNormal){
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[] {android.R.attr.state_pressed},new ColorDrawable(getColor(context,statePressed)));
        states.addState(new int[]{}, new ColorDrawable(getColor(context, stateNormal)));
        return states;
    }
    public static Drawable getSelector2(Context context){
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[] {android.R.attr.state_pressed},new ColorDrawable(getColor(context,R.attr.colorControlHighlight)));
        states.addState(new int[] {android.R.attr.state_activated},new ColorDrawable(getColor(context,R.attr.colorControlActivated)));
        states.addState(new int[] {android.R.attr.state_checked},new ColorDrawable(getColor(context,R.attr.colorControlActivated)));
        states.addState(new int[]{}, new ColorDrawable(getColor(context,R.attr.colorPrimary)));
        return states;
    }
    public static void setTheme(Activity activity){
        SharedPreferences sharedPreferences = activity.getSharedPreferences("theme",activity.MODE_PRIVATE);
        if(sharedPreferences.getInt("theme",0)==0)return;
        activity.setTheme(sharedPreferences.getInt("theme",0));
    }
    public static void changeTheme(Activity activity,int theme){
        SharedPreferences sharedPreferences = activity.getSharedPreferences("theme", activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("theme", theme);
        editor.commit();
        activity.finish();
        activity.startActivity(activity.getIntent());
    }
}
